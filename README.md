Grafana_docker role
=========

Installs grafana docker-compose project

Requirements
------------

—

Role Variables
--------------

See defaults/main/*

Dependencies
------------

—

Example Playbook
----------------

```
---
- name: Install grafana
  hosts: monitoring_vms
  become: true
  gather_facts: true
  roles:
    - grafana_docker
      tags:
        - role_grafana_docker
```

License
-------

MIT

Author Information
------------------

n98gt56ti@gmail.com
